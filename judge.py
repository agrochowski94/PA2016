import importlib
import glob
import subprocess
import sys
from time import time

task_code = sys.argv[1]
task_module = importlib.import_module('tasks.{}'.format(task_code.upper()))

def exact_judge(result, expected_result, *args):
    return result.strip().split(' ') == expected_result.strip().split(' ')

compile_command = getattr(
    task_module,
    'compile_command',
    'g++ tasks/{upper}/{lower}.cpp -o tasks/{upper}/{lower}'
).format(
    upper=task_code.upper(),
    lower=task_code.lower()
)
judge = getattr(task_module, 'judge', exact_judge)
if judge is exact_judge:
    print 'Using \033[91mexact\033[0m judge'
else:
    print 'Using \033[93mcustom\033[0m judge'

print 'COMPILING {}'.format(compile_command)
compile_results = subprocess.check_output(compile_command.split(' '))
print 'COMPILED {}'.format(compile_results)
tests = glob.glob('tasks/{}/tests/**/*.in'.format(task_code))
ok_amount = 0

for test in tests:
    run_command = getattr(task_module, 'run_command', './tasks/{upper}/{lower}')
    run_command = run_command.format(
        upper=task_code.upper(),
        lower=task_code.lower()
    )
    print run_command
    start = time() * 1000

    try:
        result = subprocess.check_output(
            run_command.split(' '),
            stdin=open(test, 'r')
        )
    except Exception as e:
        print str(e)
    else:
        print '{} {} ms'.format(test, time()*1000 - start),
        if '--save' in sys.argv:
            with open('{}.out'.format(test[:-3]), 'w') as expected_result:
                expected_result.write(result)
            print ''
        else:
            with open('{}.out'.format(test[:-3]), 'r') as expected_result:
                if judge(result, expected_result.read(), open(test, 'r').read()):
                    ok_amount += 1
                    print '\033[92mOK\033[0m'
                else:
                    print '\033[91mNOT OK\033[0m'

print '\033[92mOK\033[0m - {}'.format(ok_amount)
