#include <stdio.h>

bool Ba[100001], Bi[100001];

int main() {
    long t;
    scanf("%ld", &t);
    
    for(int i=0; i<t; i++) {
        long n, m;
        long ba_sum = 0, bi_sum = 0;
        scanf("%ld %ld", &n, &m);
        for(int j=0; j<n; j++) {
            Ba[j] = false;
            Bi[j] = false;
        }
        for(int j=0; j<m; j++) {
            long a, b;
            char w;
            scanf("%ld %c %ld", &a, &w, &b);
            if (w == '>') {
                if (!Ba[a]) {
                    Ba[a] = true;
                    ba_sum++;
                }
            } else {
                if (!Bi[b]) {
                    Bi[b] = true;
                    bi_sum++;
                }               
            }
        }

        if (ba_sum >= n) {
            puts("WYGRANA");
        } else if (bi_sum >= n) {
            puts("PRZEGRANA");
        } else {
            puts("REMIS");
        }
    }

    return 0;
};