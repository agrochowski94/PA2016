import random

for j in xrange(100):
    with open('{}.in'.format(j), 'w') as test:
        n = 10
        m = n - 1
        test.write('{} {}\n'.format(n, m))
        for i in xrange(0, n):
            test.write('{} '.format(random.randint(9, 100)))

        test.write('\n')

        for i in xrange(0, m):
            test.write('{} '.format(random.randint(1, 10)))

    with open('{}.out'.format(j), 'w') as test:
        test.write('\n')
