def judge(result, expected_result, *args):
    return result.strip().split(' ') == expected_result.strip().split(' ')