#include <stdio.h>

int main() {
    long n, t, *a, a_size;
    scanf("%ld %ld", &n, &t);
    a_size = 1<<n;
    a = new long [a_size];

    for(long i=0; i<a_size; i++) {
        scanf("%ld", &a[i]);
    }

    if (t % 2) {
        for (long i = a_size - 1; i >= 0; i--) {
            printf("%ld ", a[i]);          
        }
    } else {
        for (long i = 0; i < a_size; ++i) {
            printf("%ld ", a[i]); 
        }
    }
    delete[] a;
    return 0;
};
