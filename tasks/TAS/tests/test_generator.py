import random

for i in xrange(10):
    with open('tests/{}.in'.format(i), 'w') as test:
        n = random.randint(19, 20)
        test.write('{} {}\n'.format(
            n,
            random.randint(1, 1000000000)
        ))
        for j in xrange(pow(2, n)):
            test.write('{} '.format(random.randint(0, 1000000000)))