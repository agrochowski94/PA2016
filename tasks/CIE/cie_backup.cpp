#include <stdio.h>
#include <utility>
#include <vector>
#include "cielib.h"

int main() {
    long d = podajD();
    long k = podajK();
    long r = podajR();
    std::vector<bool> ready(d, false);
    long ready_amount = 0;

    std::vector<int> coordinates(d, r/2);
    std::vector<std::pair<int, int>> b_search(
        d,
        std::pair<int, int>(0, r)
    );
    czyCieplo(&coordinates[0]);

    int t=100;
    while(ready_amount != d) {
        for(int i=0; i<d; i++) {
            if (!ready[i]) {
                std::vector<int> left(coordinates);
                std::vector<int> right(coordinates);
                left[i] = b_search[i].first;
                right[i] = b_search[i].second;
                coordinates[i] = (b_search[i].first + b_search[i].second)/2;
                if (i == 1) {
                    //printf("%d %d %d %d %d %d\n", b_search[i].first, coordinates[i], b_search[i].second, hot_left, hot_center, hot_right);
                }

                if (czyCieplo(&left[0])) {
                    if (b_search[i].second - b_search[i].first == 2) {
                        coordinates[i] = b_search[i].first;
                        ready[i] = true;
                        ready_amount++;
                    }
                    b_search[i].second = (b_search[i].first + b_search[i].second)/2;
                    coordinates = left;
                } else if (czyCieplo(&right[0])) {
                    if (b_search[i].second - b_search[i].first == 2) {
                        coordinates[i] = b_search[i].second;
                        ready[i] = true;
                        ready_amount++;
                    }
                    b_search[i].first = (b_search[i].first + b_search[i].second)/2;
                    coordinates = right;
                } else if (czyCieplo(&coordinates[0])) {
                    if (b_search[i].second - b_search[i].first == 2) {  
                        ready[i] = true;
                        ready_amount++;
                    }
                    b_search[i].second = (b_search[i].first + b_search[i].second)/2;
                }
                if ((b_search[i].second - b_search[i].first) % 2) {
                    if (b_search[i].first) {
                        b_search[i].first--;
                    } else {
                        b_search[i].second++;
                    }
                }
            }
        }
    }
    znalazlem(&coordinates[0]);

    return 0;
};
