task_code = 'CIE'

compile_command = 'g++ -std=c++11 -static -s tasks/{upper}/cielib.c tasks/{upper}/{lower}.cpp -o tasks/{upper}/{lower}'.format(
    upper=task_code.upper(),
    lower=task_code.lower()
)

def judge(result, expected_result, *args):
    return 'OK' in result