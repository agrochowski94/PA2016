import random

for j in xrange(1000):
    with open('{}.in'.format(j), 'w') as test:
        d = 5
        r = random.randint(2, 100)
        test.write('{}\n'.format(d))
        test.write('{}\n'.format(d*100))
        test.write('{}\n'.format(r))
        for i in xrange(0, d):
            test.write('{} '.format(random.randint(0, r)))

    with open('{}.out'.format(j), 'w') as test:
        test.write('\n')
