#include <stdio.h>
#include <utility>
#include <vector>
#include "cielib.h"

int main() {
    long d = podajD();
    long k = podajK();
    long r = podajR();
    std::vector<bool> ready(d, false);
    long ready_amount = 0;

    std::vector<int> coordinates(d, r/2);
    std::vector<std::pair<int, int>> b_search(
        d,
        std::pair<int, int>(0, r)
    );
    czyCieplo(&coordinates[0]);

    while(ready_amount != d) {
        for(int i=0; i<d; i++) {
            if (!ready[i]) {
                int middle = coordinates[i];
                bool is_odd = (b_search[i].second-b_search[i].first) % 2;
                std::vector<int> left(coordinates);
                std::vector<int> right(coordinates);
                left[i] = b_search[i].first;
                right[i] = b_search[i].second;
                if (right[i] - left[i] <= 2) {
                    ready[i] = true;
                    ready_amount++;
                    if (czyCieplo(&left[0])) {
                        coordinates[i] = left[i];
                    } else if (czyCieplo(&right[0])) {
                        coordinates[i] = right[i];
                    } else if (czyCieplo(&left[0])) {
                        coordinates[i] = left[i];                        
                    }
                } else {
                    if (czyCieplo(&left[0])) {
                        b_search[i].second = middle;
                    } else if (czyCieplo(&right[0])) {
                        b_search[i].first = middle;
                        if (is_odd) {
                            b_search[i].first++;
                        }
                    } else if(czyCieplo(&left[0])) {
                        b_search[i].second = middle;
                    } else {
                        if (is_odd) {
                            b_search[i].first = middle;
                        } else {
                            ready[i] = true;
                            ready_amount++;   
                        }
                    }
                }

                if (b_search[i].second - b_search[i].first == 1) {
                    if (b_search[i].first)
                        b_search[i].first--;
                    else
                        b_search[i].second++;
                }
            }
        }
        for(int i=0; i<d; i++) {
            if (!ready[i]) {
                coordinates[i] = (b_search[i].first + b_search[i].second)/2;
            }
        }
        czyCieplo(&coordinates[0]);
    }
    znalazlem(&coordinates[0]);

    return 0;
};
