import random

SEGMENT_SIZE = 10000
START = 0

for j in xrange(100):
    with open('{}.in'.format(j), 'w') as test:
        test.write('{}\n'.format(SEGMENT_SIZE))
        for i in xrange(0, SEGMENT_SIZE):
            test.write('{}\n'.format(random.randint(1, 1000000000)))

    with open('{}.out'.format(j), 'w') as test:
        test.write('\n')
