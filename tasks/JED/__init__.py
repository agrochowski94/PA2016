def judge(result, expected_result, input_data):
    test_inputs = input_data.split('\n')[1:-1]
    status = True
    test_cases = result.split('\n')
    for id, test_case in enumerate(test_cases[:-1]):
        counter = test_case.count('1')
        if eval(test_case) != int(test_inputs[id]) or counter > 100:
            status = False

    return status and not '(1)' in result and '((1+1))' not in result