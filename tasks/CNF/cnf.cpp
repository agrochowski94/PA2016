#include <cstdio>
#include <set>
#include <vector>

char read_int(int *n){register char c=0;while(c<33)c=getc_unlocked(stdin);(*n)=0;while(c>47&&c<58){(*n)=(*n)*10+(c-'0');c=getc_unlocked(stdin);}return c;}

int main() {
    int n, x;
    read_int(&n);
    char c, last_character;

    std::vector<std::set<int>> clauses(n);
    while(c != EOF) {
        putc_unlocked(c, stdout);
        if (c == 'x') {
            last_character = read_int(&x);
            putc_unlocked(last_character, stdout);
        }
        c = getc_unlocked(stdin);
    }
    printf(" %d ", x);
    // putc_unlocked(getc_unlocked(stdin), stdout);
}